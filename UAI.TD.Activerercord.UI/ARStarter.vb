﻿Imports Castle.ActiveRecord.Framework.Config
Imports Castle.ActiveRecord
Imports System.Reflection

Public Class ARStarter

    Public Shared Sub Start()
        Dim source As New InPlaceConfigurationSource

        Dim properties As New Dictionary(Of String, String)
        properties.Add("hibernate.connection.driver_class", "NHibernate.Driver.SqlClientDriver")
        properties.Add("dialect", "NHibernate.Dialect.MsSql2000Dialect")
        properties.Add("hibernate.connection.provider", "NHibernate.Connection.DriverConnectionProvider")
        properties.Add("connection.connection_string", "Data Source=.;Initial Catalog=test;Integrated Security=SSPI")
        properties.Add("proxyfactory.factory_class", "NHibernate.ByteCode.Castle.ProxyFactoryFactory, NHibernate.ByteCode.Castle")
        source.Add(GetType(ActiveRecordBase), properties)

        Dim asm As Assembly = Assembly.Load("UAI.TD.Activerercord.UI")

        ActiveRecordStarter.Initialize(asm, source)
        ActiveRecordStarter.UpdateSchema()


    End Sub

End Class
