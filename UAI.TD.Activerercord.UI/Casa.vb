﻿Imports Castle
Imports Castle.ActiveRecord

<[ActiveRecord]>
Public Class Casa
    Inherits ActiveRecordBase(Of Casa)

    Public Sub New()
        Habitaciones = New List(Of Habitacion)
    End Sub

    <PrimaryKey(PrimaryKeyType.Native, "id")>
    Public Overridable Property Id As Long


    <[HasMany]>
    Public Property Habitaciones As IList(Of Habitacion)

    <[Property]>
    Public Property Nombre As String

End Class
