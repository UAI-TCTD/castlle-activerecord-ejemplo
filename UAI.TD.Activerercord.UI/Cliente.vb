﻿Imports Castle.ActiveRecord



<ActiveRecord>
Public Class Cliente
    Inherits ActiveRecordBase(Of Cliente)


    <PrimaryKey(PrimaryKeyType.Native, "id")>
    Public Overridable Property Id As Long

    <[Property]>
    Public Overridable Property Nombre As String


End Class
