﻿
Imports Castle.ActiveRecord

<ActiveRecord>
Public Class Habitacion
    Inherits ActiveRecordBase(Of Habitacion)
    <PrimaryKey(PrimaryKeyType.Native, "id")>
    Public Overridable Property Id As Long

    <[Property]>
    Public Property Nombre As String

    <BelongsTo>
    Public Property Casa As Casa
End Class
